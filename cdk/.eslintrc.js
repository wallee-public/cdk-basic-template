module.exports = {
  root: true,
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/eslint-recommended', 'plugin:@typescript-eslint/recommended', 'prettier'],
  env: {
    es2021: true
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.eslint.json',
  },
  plugins: ['@typescript-eslint'],
  rules: {
    'no-console': 'warn',
    'import/prefer-default-export': 'off',
    'no-unused-vars': [
      'warn', {}
    ],
    '@typescript-eslint/no-unused-vars': [
      'warn', {}
    ]
  }
};