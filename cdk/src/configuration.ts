<#assign nameTag = name?replace(" ", "_")?upper_case>

// Repo name
export const CODECOMMIT_REPO_NAME = '${codeCommitRepoName}';

// Account numbers
export const DEV_OPS_ACCOUNT = '${devOpsAccountId!"679651338820"}';
export const STAGING_COMMON_SERVICES_ACCOUNT = '${stagingCommonServicesAccountId!"915522256379"}';
export const PRODUCTION_COMMON_SERVICES_ACCOUNT = '${prodCommonServicesAccountId!"347048086303"}';

// Slack
export const SLACK_PIPELINE_NOTIFICATION_CHANNEL_CONFIGURATION_ARN =
	'arn:aws:chatbot::679651338820:chat-configuration/slack-channel/pipeline-notifications-channel';
export const SLACK_NOTIFICATIONS_ENABLED = ${slackNotificationsEnabled!"false"};

// AWS region
export const REGION = 'eu-west-1';

// Logs
export const LOG_GROUP_NAME = '${logGroupName}';
export const LOG_STREAM_PREFIX = '${logStreamPrefix}';

// Tags
export const LOAD_BALANCER_NAME_TAG_KEY = 'Name';
export const LOAD_BALANCER_NAME_TAG_VALUE = 'common-services-elb';

// Ports
export const ${nameTag}_SERVER_PORT = ${serverPort};
<#if features?filter(feature -> feature.id=="mysql")?size != 0>
export const MYSQL_DB_PORT = 3306;
</#if>
<#if features?filter(feature -> feature.id=="postgresql")?size != 0>
export const POSTGRESQL_DB_PORT = 5432;
</#if>

// Timezone
export const TIMEZONE = 'Europe/Zurich';

// Parameter names
export const ${nameTag}_URL_PARAMETER = '${nameTag}_URL';
<#if features?filter(feature -> feature.id=="saml")?size != 0>
export const SP_ENTITY_ID_PARAMETER = '${nameTag}_SAML_SERVICE_ENTITY_ID';
export const IDP_ENTITY_ID_PARAMETER = 'SAML_IDP_ENTITY_ID';
export const IDP_SSO_URL_PARAMETER = 'SAML_IDP_SSO_URL';
export const IDP_X509_CERTIFICATE_PARAMETER = 'SAML_IDP_X509_CERTIFICATE';
</#if>
<#if features?filter(feature -> feature.id=="mysql")?size != 0 || features?filter(feature -> feature.id=="mysql")?size != 0>
export const DB_HOST_PARAMETER = '${nameTag}_DB_HOST';
export const DB_NAME_PARAMETER = '${nameTag}_DB_NAME';
export const DB_PASSWORD_PARAMETER = '${nameTag}_DB_PASSWORD';
export const DB_USERNAME_PARAMETER = '${nameTag}_DB_USERNAME';
</#if>
export const PIPELINE_APPROVAL_BOT_SNS_TOPIC_ARN_PARAMETER = 'PIPELINE_APPROVAL_BOT_SNS_TOPIC_ARN';
export const NEXT_ALB_RULE_PRIORITY_SERVICE_TOKEN_PARAMETER = 'NEXT_ALB_RULE_PRIORITY_SERVICE_TOKEN';

// Secrets
export const DOCKER_HUB_CREDENTIALS_SECRET = 'DOCKER_HUB_CREDENTIALS';

<#if features?filter(feature -> feature.id=="mysql")?size != 0>
export const MYSQL_COMMON_SERVICES_SECURITY_GROUP_NAME = 'mysql-common-services-sg';
</#if>
<#if features?filter(feature -> feature.id=="postgresql")?size != 0>
export const POSTGRESQL_COMMON_SERVICES_SECURITY_GROUP_NAME = 'postgresql-common-services-sg';
</#if>