<#assign nameTag = name?replace(" ", "_")?upper_case>
<#assign nameCcPrefix = name?replace(" ", "")>
<#assign nameDcPrefix = name?replace(" [a-zA-Z]", "-$0", 'r')?replace(" ", "")?lower_case>

import { CfnOutput, RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';
import { Repository, TagMutability } from 'aws-cdk-lib/aws-ecr';
import { DockerImageAsset } from 'aws-cdk-lib/aws-ecr-assets';
import { ArnPrincipal, Effect, PolicyStatement } from 'aws-cdk-lib/aws-iam';
import { DockerImageName, ECRDeployment } from 'cdk-ecr-deployment';
import { Construct } from 'constructs';
import { PRODUCTION_COMMON_SERVICES_ACCOUNT, STAGING_COMMON_SERVICES_ACCOUNT } from '../configuration';
import { createDockerImageTag } from '../docker-image.util';

export interface IDockerImageCfnOutput {
	readonly imageTag: CfnOutput;
	readonly repositoryName: CfnOutput;
	readonly repositoryArn: CfnOutput;
}

export class ${nameCcPrefix}DockerImageStack extends Stack {
	public dockerImageCfnOutput: IDockerImageCfnOutput;

	constructor(scope: Construct, id: string, props: StackProps) {
		super(scope, id, props);

		const imageAsset = this.createDockerImageAsset();
		const repository = this.createEcrRepository();
		const commitTag = createDockerImageTag();

		new ECRDeployment(this, 'copy-docker-image-to-ecr', {
			src: new DockerImageName(imageAsset.imageUri),
			dest: new DockerImageName(`${r"${repository.repositoryUri}"}:${r"${commitTag}"}`)
		});

		this.dockerImageCfnOutput = {
			imageTag: new CfnOutput(this, 'image-tag', { value: commitTag }),
			repositoryName: new CfnOutput(this, 'repository-name', { value: repository.repositoryName }),
			repositoryArn: new CfnOutput(this, 'repository-arn', { value: repository.repositoryArn })
		};
	}

	private createDockerImageAsset(): DockerImageAsset {
		return new DockerImageAsset(this, '${nameDcPrefix}-image', { directory: '../', exclude: ['cdk'] });
	}

	private createEcrRepository(): Repository {
		const repository = new Repository(this, '${nameDcPrefix}', {
			repositoryName: '${nameDcPrefix}',
			removalPolicy: RemovalPolicy.DESTROY,
			imageScanOnPush: true,
			imageTagMutability: TagMutability.IMMUTABLE,
			lifecycleRules: [
				{
					description: '${name} ECR repoitory lifecycle rule (max 5 images).',
					maxImageCount: 5
				}
			]
		});

		// Other acccounts need to be able pull images from DevOps account.
		repository.addToResourcePolicy(
			new PolicyStatement({
				sid: 'AllowCrossAccountPull',
				effect: Effect.ALLOW,
				principals: [
					new ArnPrincipal(`arn:aws:iam::${r"${STAGING_COMMON_SERVICES_ACCOUNT}"}:root`),
					new ArnPrincipal(`arn:aws:iam::${r"${PRODUCTION_COMMON_SERVICES_ACCOUNT}"}:root`)
				],
				actions: ['ecr:GetDownloadUrlForLayer', 'ecr:BatchCheckLayerAvailability', 'ecr:BatchGetImage']
			})
		);

		return repository;
	}
}
