<#assign nameTag = name?replace(" ", "_")?upper_case>
<#assign nameCcPrefix = name?replace(" ", "")>
<#assign nameDcPrefix = name?replace(" [a-zA-Z]", "-$0", 'r')?replace(" ", "")?lower_case>
import { CustomResource, Duration, RemovalPolicy, Stack, StackProps, Token } from 'aws-cdk-lib';
import { IVpc, SecurityGroup, Vpc } from 'aws-cdk-lib/aws-ec2';
import { Repository } from 'aws-cdk-lib/aws-ecr';
import {
	AwsLogDriver,
	Cluster,
	EcrImage,
	FargateService,
	FargateTaskDefinition,
	Protocol,
	Secret
} from 'aws-cdk-lib/aws-ecs';
import * as elb from 'aws-cdk-lib/aws-elasticloadbalancingv2';
import {
	ApplicationListener,
	ApplicationListenerRule,
	ApplicationProtocolVersion,
	ApplicationTargetGroup,
	CfnListenerRule,
	IApplicationListener,
	ListenerCondition,
	TargetGroupLoadBalancingAlgorithmType
} from 'aws-cdk-lib/aws-elasticloadbalancingv2';
import { Port } from "aws-cdk-lib/aws-ec2";
import { ManagedPolicy, Role, ServicePrincipal } from 'aws-cdk-lib/aws-iam';
import { LogGroup, RetentionDays } from 'aws-cdk-lib/aws-logs';
import { StringParameter } from 'aws-cdk-lib/aws-ssm';
import { Construct } from 'constructs';
import {
	LOAD_BALANCER_NAME_TAG_KEY,
	LOAD_BALANCER_NAME_TAG_VALUE,
	LOG_GROUP_NAME,
	LOG_STREAM_PREFIX,
	${nameTag}_SERVER_PORT,
	${nameTag}_URL_PARAMETER,
	<#if features?filter(feature -> feature.id=="saml")?size != 0>
	SP_ENTITY_ID_PARAMETER,
	IDP_ENTITY_ID_PARAMETER,
	IDP_SSO_URL_PARAMETER,
	IDP_X509_CERTIFICATE_PARAMETER,
	</#if>
	<#if features?filter(feature -> feature.id=="mysql")?size != 0>
	MYSQL_DB_PORT,
	MYSQL_COMMON_SERVICES_SECURITY_GROUP_NAME,
	DB_HOST_PARAMETER,
    DB_NAME_PARAMETER,
    DB_PASSWORD_PARAMETER,
    DB_USERNAME_PARAMETER,
	</#if>
	<#if features?filter(feature -> feature.id=="postgresql")?size != 0>
	POSTGRESQL_DB_PORT,
	POSTGRESQL_COMMON_SERVICES_SECURITY_GROUP_NAME,
	DB_HOST_PARAMETER,
    DB_NAME_PARAMETER,
    DB_PASSWORD_PARAMETER,
    DB_USERNAME_PARAMETER,
	</#if>
	TIMEZONE
} from '../configuration';
import { IDockerImageCfnOutput } from './docker-image.stack';

export interface ${nameCcPrefix}StackProps extends StackProps {
	readonly dockerImageCfnOutput: IDockerImageCfnOutput;
}

export class ${nameCcPrefix}Stack extends Stack {
	constructor(scope: Construct, id: string, props: ${nameCcPrefix}StackProps) {
		super(scope, id, props);

		const vpc = this.findApplicationVpc();
		const cluster = this.createCluster(vpc);
		const dockerImage = this.getDockerEcrImage(props);
		const logGroup = this.createLogGroup();
		const taskDefinition = this.createTaskDefinition(dockerImage, logGroup);
		const securityGroup = this.createSecurityGroup(vpc);
		const service = this.createFargateService(cluster, taskDefinition, securityGroup);
		this.createLoadBalancerListenerRule(vpc, service, props);
	}

	private findApplicationVpc(): IVpc {
		return Vpc.fromLookup(this, 'common-services', { vpcName: 'common-services' });
	}

	private createCluster(vpc: IVpc): Cluster {
		return new Cluster(this, '${nameDcPrefix}-cluster', {
			clusterName: '${nameCcPrefix}Cluster',
			containerInsights: true,
			vpc
		});
	}

	private getDockerEcrImage(props: ${nameCcPrefix}StackProps): EcrImage {
		const repository = Repository.fromRepositoryAttributes(this, '${nameDcPrefix}-image-repo', {
			repositoryArn: props.dockerImageCfnOutput.repositoryArn.value,
			repositoryName: props.dockerImageCfnOutput.repositoryName.value
		});
		return new EcrImage(repository, props.dockerImageCfnOutput.imageTag.value);
	}

	private createLogGroup(): LogGroup {
		return new LogGroup(this, '${nameDcPrefix}-log-group', {
			logGroupName: LOG_GROUP_NAME,
			removalPolicy: RemovalPolicy.DESTROY,
			retention: RetentionDays.SIX_MONTHS
		});
	}

	private createTaskDefinition(
		image: EcrImage,
		logGroup: LogGroup): FargateTaskDefinition {
		const taskDefinition = new FargateTaskDefinition(this, '${nameDcPrefix}-task', {
			cpu: ${cpu!"512"},
			memoryLimitMiB: ${memoryLimit!"1024"},
			taskRole: this.createTaskDefinitionTaskRole(),
			executionRole: this.createTaskDefinitionExecutionRole()
		});

		taskDefinition.addContainer('${nameDcPrefix}-container', {
			containerName: '${nameDcPrefix}-container',
			image,
			environment: {
				GENERIC_TIMEZONE: TIMEZONE,
				TZ: TIMEZONE,
				SERVICE_URL: StringParameter.valueForStringParameter(this, ${nameTag}_URL_PARAMETER)<#if features?filter(feature -> feature.id=="saml")?size != 0>,
				SP_ENTITY_ID: StringParameter.valueForStringParameter(this, SP_ENTITY_ID_PARAMETER),
				IDP_ENTITY_ID: StringParameter.valueForStringParameter(this, IDP_ENTITY_ID_PARAMETER),
				IDP_SSO_URL: StringParameter.valueForStringParameter(this, IDP_SSO_URL_PARAMETER),
				IDP_X509_CERTIFICATE: StringParameter.valueForStringParameter(this, IDP_X509_CERTIFICATE_PARAMETER)</#if><#if features?filter(feature -> feature.id=="postgresql")?size != 0>,
				POSTGRESQL_DB_USER: StringParameter.valueForStringParameter(this, DB_USERNAME_PARAMETER),
				POSTGRESQL_DB_HOST: StringParameter.valueForStringParameter(this, DB_HOST_PARAMETER),
				POSTGRESQL_DB_NAME: StringParameter.valueForStringParameter(this, DB_NAME_PARAMETER),
				POSTGRESQL_DB_PORT: POSTGRESQL_DB_PORT</#if><#if features?filter(feature -> feature.id=="mysql")?size != 0>,
				MYSQL_DB_USER: StringParameter.valueForStringParameter(this, DB_USERNAME_PARAMETER),
				MYSQL_DB_HOST: StringParameter.valueForStringParameter(this, DB_HOST_PARAMETER),
				MYSQL_DB_NAME: StringParameter.valueForStringParameter(this, DB_NAME_PARAMETER),
				MYSQL_DB_PORT: MYSQL_DB_PORT</#if>
			},
			logging: new AwsLogDriver({
				streamPrefix: LOG_STREAM_PREFIX,
				logGroup
			}),
			portMappings: [
				{
					containerPort: ${nameTag}_SERVER_PORT,
					hostPort: ${nameTag}_SERVER_PORT,
					protocol: Protocol.TCP
				}
			]
		});
		return taskDefinition;
	}

	private createSecurityGroup(vpc: IVpc): SecurityGroup {
		const ${nameCcPrefix?uncap_first}SecurityGroup = new SecurityGroup(this, '${nameDcPrefix}-server-sg', {
			vpc,
			allowAllOutbound: true, // FIXME
			securityGroupName: '${nameDcPrefix}-server-sg',
			description: 'Security group for wallee ${name} application.'
		});

		<#if features?filter(feature -> feature.id=="mysql")?size != 0>
		const mysqlSecurityGroup = SecurityGroup.fromLookupByName(
			this,
			'mysql-common-services-sg',
			MYSQL_COMMON_SERVICES_SECURITY_GROUP_NAME,
			vpc
		);

		mysqlSecurityGroup.addIngressRule(
			${nameCcPrefix?uncap_first}SecurityGroup,
			Port.tcp(MYSQL_DB_PORT),
			`Incoming database traffic from ${name} security group (${nameDcPrefix}-server-sg).`
		);

		${nameCcPrefix?uncap_first}SecurityGroup.addEgressRule(
			mysqlSecurityGroup,
			Port.tcp(MYSQL_DB_PORT),
			`Outgoing database traffic to common MySQL database security group.`
		);
		</#if>
		<#if features?filter(feature -> feature.id=="postgresql")?size != 0>
		const postgresqlSecurityGroup = SecurityGroup.fromLookupByName(
			this,
			'postgresql-common-services-sg',
			POSTGRESQL_COMMON_SERVICES_SECURITY_GROUP_NAME,
			vpc
		);

		postgresqlSecurityGroup.addIngressRule(
			${nameCcPrefix?uncap_first}SecurityGroup,
			Port.tcp(POSTGRESQL_DB_PORT),
			`Incoming database traffic from ${name} security group (${nameDcPrefix}-server-sg).`
		);

		${nameCcPrefix?uncap_first}SecurityGroup.addEgressRule(
			postgresqlSecurityGroup,
			Port.tcp(POSTGRESQL_DB_PORT),
			`Outgoing database traffic to common PostgreSQL database security group.`
		);
		</#if>

		return ${nameCcPrefix?uncap_first}SecurityGroup;
	}

	private createFargateService(
		cluster: Cluster,
		taskDefinition: FargateTaskDefinition,
		securityGroup: SecurityGroup
	): FargateService {
		return new FargateService(this, '${nameDcPrefix}-service', {
			serviceName: '${nameDcPrefix}-service',
			cluster,
			taskDefinition,
			desiredCount: 1,
			minHealthyPercent: 100,
			maxHealthyPercent: 200,
			assignPublicIp: true,
			securityGroups: [securityGroup]
		});
	}

	private createLoadBalancerListenerRule(vpc: IVpc, service: FargateService, props: ${nameCcPrefix}StackProps) {
		const group = new ApplicationTargetGroup(this, '${nameDcPrefix}-target-group', {
			vpc,
			targetGroupName: '${nameDcPrefix}-target-group',
			targets: [service],
			port: ${nameTag}_SERVER_PORT,
			protocol: elb.ApplicationProtocol.HTTP,
			protocolVersion: ApplicationProtocolVersion.HTTP1,
			healthCheck: {
				protocol: elb.Protocol.HTTP,
				path: '/',
				healthyThresholdCount: 2,
				unhealthyThresholdCount: 5,
				timeout: Duration.seconds(10),
				interval: Duration.seconds(12),
				healthyHttpCodes: '200-399'
			},
			deregistrationDelay: Duration.seconds(300),
			loadBalancingAlgorithmType: TargetGroupLoadBalancingAlgorithmType.ROUND_ROBIN
		});

		const listener = ApplicationListener.fromLookup(this, 'common-services-elb-listener-https-443', {
			listenerPort: 443,
			loadBalancerTags: {
				[LOAD_BALANCER_NAME_TAG_KEY]: LOAD_BALANCER_NAME_TAG_VALUE
			}
		});

		const listenerRule = new ApplicationListenerRule(this, '${nameDcPrefix}-listener-rule', {
			listener: listener,
			priority: 9999,
			conditions: [ListenerCondition.hostHeaders([StringParameter.valueForStringParameter(this, ${nameTag}_URL_PARAMETER)])],
			targetGroups: [group]
		});

		const nextListenerRulePriority = this.getNextListenerRulePriority(listener);
		(listenerRule.node.defaultChild as CfnListenerRule).priority = Token.asNumber(nextListenerRulePriority);

	}

	private createTaskDefinitionExecutionRole(): Role {
		return new Role(this, '${nameDcPrefix}-task-definition-execution-role', {
			roleName: '${nameDcPrefix}-task-definition-execution-role',
			assumedBy: new ServicePrincipal('ecs-tasks.amazonaws.com'),
			managedPolicies: [
				ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonECSTaskExecutionRolePolicy'),
				ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMReadOnlyAccess')
			]
		});
	}

	private createTaskDefinitionTaskRole(): Role {
		return new Role(this, '${nameDcPrefix}-task-definition-task-role', {
			roleName: '${nameDcPrefix}-task-definition-task-role',
			assumedBy: new ServicePrincipal('ecs-tasks.amazonaws.com'),
			managedPolicies: []
		});
	}

	private getSecret(parameterName: string): Secret {
		return Secret.fromSsmParameter(
			StringParameter.fromSecureStringParameterAttributes(this, `${r"${parameterName}"}-id`, { parameterName })
		);
	}

	private getNextListenerRulePriority(listener: IApplicationListener): string {
		const lambda = new CustomResource(this, 'next-listener-rule-priority', {
			resourceType: 'Custom::NextListenerRulePriority',
			serviceToken: StringParameter.valueForStringParameter(this, NEXT_ALB_RULE_PRIORITY_SERVICE_TOKEN_PARAMETER),
			properties: {
				ListenerArn: listener.listenerArn
			}
		});
		return lambda.getAttString('Priority');
	}
}
