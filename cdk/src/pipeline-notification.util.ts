import { SlackChannelConfiguration } from 'aws-cdk-lib/aws-chatbot';
import { PipelineNotificationEvents } from 'aws-cdk-lib/aws-codepipeline';
import { Topic } from 'aws-cdk-lib/aws-sns';
import { CodePipeline } from 'aws-cdk-lib/pipelines';
import { Construct } from 'constructs';

/**
 * Sets up Slack notifications for code pipeline.
 *
 * @param scope the scope where this should be applied.
 * @param pipeline the code pipeline which should have the notifications.
 * @param idPrefix the prefix of id and names. Example: 'my-service-pipeline'.
 * @param slackChannelConfigurationArn the ARN of the Slack channel configuration.
 * @param pipelineApprovalBotSnsTopicArn the ARN of the Slack approval bot SNS topic.
 */
export const setUpPipelineNotifications = (
	scope: Construct,
	pipeline: CodePipeline,
	idPrefix: string,
	slackChannelConfigurationArn: string,
	pipelineApprovalBotSnsTopicArn: string
): void => {
	// Pipeline AWS ChatBot notifications
	const notificationTarget = SlackChannelConfiguration.fromSlackChannelConfigurationArn(
		scope,
		`${r"${idPrefix}"}-slack-channel-configuration`,
		slackChannelConfigurationArn
	);

	pipeline.pipeline.notifyOn(`${r"${idPrefix}"}-notification-rule`, notificationTarget, {
		notificationRuleName: `${r"${idPrefix}"}-notification-rule`,
		events: [
			PipelineNotificationEvents.PIPELINE_EXECUTION_STARTED,
			PipelineNotificationEvents.PIPELINE_EXECUTION_FAILED,
			PipelineNotificationEvents.PIPELINE_EXECUTION_CANCELED,
			PipelineNotificationEvents.PIPELINE_EXECUTION_SUCCEEDED
		]
	});

	// Pipeline Approval Bot notfications
	const topic = Topic.fromTopicArn(scope, 'pipeline-approval-bot-sns-topic', pipelineApprovalBotSnsTopicArn);

	pipeline.pipeline.notifyOn(`${r"${idPrefix}"}-approval-notification-rule`, topic, {
		notificationRuleName: `${r"${idPrefix}"}-approval-notification-rule`,
		events: [PipelineNotificationEvents.MANUAL_APPROVAL_NEEDED]
	});
};
