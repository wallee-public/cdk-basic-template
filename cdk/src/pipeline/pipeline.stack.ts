<#assign nameTag = name?replace(" ", "_")?upper_case>
<#assign nameCcPrefix = name?replace(" ", "")>
<#assign nameDcPrefix = name?replace(" [a-zA-Z]", "-$0", 'r')?replace(" ", "")?lower_case>
import { App, Duration, Stack, StackProps, Stage, StageProps } from 'aws-cdk-lib';
import { Repository } from 'aws-cdk-lib/aws-codecommit';
import { Rule, Schedule } from 'aws-cdk-lib/aws-events';
import * as targets from 'aws-cdk-lib/aws-events-targets';
import { PolicyStatement } from 'aws-cdk-lib/aws-iam';
import { Secret } from 'aws-cdk-lib/aws-secretsmanager';
import { StringParameter } from 'aws-cdk-lib/aws-ssm';
import {
	CodePipeline,
	CodePipelineSource,
	DockerCredential,
	ManualApprovalStep,
	ShellStep
} from 'aws-cdk-lib/pipelines';
import { Construct } from 'constructs';
import {
	DEV_OPS_ACCOUNT,
	DOCKER_HUB_CREDENTIALS_SECRET,
	PIPELINE_APPROVAL_BOT_SNS_TOPIC_ARN_PARAMETER,
	PRODUCTION_COMMON_SERVICES_ACCOUNT,
	REGION,
	SLACK_NOTIFICATIONS_ENABLED,
	SLACK_PIPELINE_NOTIFICATION_CHANNEL_CONFIGURATION_ARN,
	STAGING_COMMON_SERVICES_ACCOUNT
} from '../configuration';
import { setUpPipelineNotifications } from '../pipeline-notification.util';
import { IDockerImageCfnOutput, ${nameCcPrefix}DockerImageStack } from '../stack/docker-image.stack';
import { ${nameCcPrefix}Stack } from '../stack/application.stack';

export interface PipelineStackProps extends StackProps {
	readonly repositoryName: string;
}

interface EnvironmentStageProps extends StageProps {
	readonly dockerImageCfnOutput: IDockerImageCfnOutput;
}

class DockerImageStage extends Stage {
	public readonly dockerImageStack: ${nameCcPrefix}DockerImageStack;

	constructor(scope: Construct, id: string, props: StageProps) {
		super(scope, id, props);
		this.dockerImageStack = new ${nameCcPrefix}DockerImageStack(this, '${nameDcPrefix}-docker-image-stack', {
			...props,
			stackName: '${nameCcPrefix}DockerImageStack'
		});
	}
}

class ${nameCcPrefix}EnvironmentStage extends Stage {
	public readonly ${nameCcPrefix?uncap_first}Stack: ${nameCcPrefix}Stack;

	constructor(scope: Construct, id: string, props: EnvironmentStageProps) {
		super(scope, id, props);
		this.${nameCcPrefix?uncap_first}Stack = new ${nameCcPrefix}Stack(this, '${nameDcPrefix}-stack', {
			...props,
			stackName: '${nameCcPrefix}Stack'
		});
	}
}

export class PipelineStack extends Stack {
	constructor(app: App, id: string, props: PipelineStackProps) {
		super(app, id, props);

		const repository = Repository.fromRepositoryName(this, 'imported-repo', props.repositoryName);

		const pipeline = new CodePipeline(this, '${nameDcPrefix}-pipeline', {
			pipelineName: '${nameCcPrefix}Pipeline',
			crossAccountKeys: true,
			dockerCredentials: [
				DockerCredential.dockerHub(Secret.fromSecretNameV2(this, 'docker-hub-secret', DOCKER_HUB_CREDENTIALS_SECRET))
			],
			synth: new ShellStep('synth', {
				input: CodePipelineSource.codeCommit(repository, 'master'),
				commands: ['cd cdk', 'npm i -g npm', 'npm ci', 'npx cdk synth --verbose'],
				primaryOutputDirectory: 'cdk/cdk.out'
			}),
			codeBuildDefaults: {
				rolePolicy: [PipelineStack.createAssumeRolePolicy()]
			}
		});

		const dockerImageStage = new DockerImageStage(this, 'DockerImage', {
			env: {
				account: DEV_OPS_ACCOUNT,
				region: REGION
			}
		});

		const stagingEnvironmentStage = new ${nameCcPrefix}EnvironmentStage(this, 'Staging', {
			env: {
				account: STAGING_COMMON_SERVICES_ACCOUNT,
				region: REGION
			},
			dockerImageCfnOutput: dockerImageStage.dockerImageStack.dockerImageCfnOutput
		});
		const productionEnvironmentStage = new ${nameCcPrefix}EnvironmentStage(this, 'Production', {
			env: {
				account: PRODUCTION_COMMON_SERVICES_ACCOUNT,
				region: REGION
			},
			dockerImageCfnOutput: dockerImageStage.dockerImageStack.dockerImageCfnOutput
		});

		pipeline.addStage(dockerImageStage);
		pipeline.addStage(stagingEnvironmentStage, {
			stackSteps: [{ stack: stagingEnvironmentStage.${nameCcPrefix?uncap_first}Stack, changeSet: [new ManualApprovalStep('DeployToStaging')] }]
		});
		pipeline.addStage(productionEnvironmentStage, {
			stackSteps: [
				{ stack: productionEnvironmentStage.${nameCcPrefix?uncap_first}Stack, changeSet: [new ManualApprovalStep('DeployToProduction')] }
			]
		});

		// We need to build pipeline before we can use other constructs with pipeline.
		pipeline.buildPipeline();

		if (SLACK_NOTIFICATIONS_ENABLED) {
			setUpPipelineNotifications(
				this,
				pipeline,
				'${nameDcPrefix}-pipeline',
				SLACK_PIPELINE_NOTIFICATION_CHANNEL_CONFIGURATION_ARN,
				StringParameter.valueForStringParameter(this, PIPELINE_APPROVAL_BOT_SNS_TOPIC_ARN_PARAMETER)
			);
		}

		// Rule to trigger pipeline in every 30 days.
		new Rule(this, 'run-${nameDcPrefix}-pipeline-monthly-rule', {
			ruleName: 'run-${nameDcPrefix}-pipeline-monthly-rule',
			schedule: Schedule.rate(Duration.days(30)),
			targets: [new targets.CodePipeline(pipeline.pipeline)]
		});
	}

	private static createAssumeRolePolicy(): PolicyStatement {
		return new PolicyStatement({
			actions: ['sts:AssumeRole'],
			resources: ['*'],
			conditions: {
				StringEquals: {
					'iam:ResourceTag/aws-cdk:bootstrap-role': 'lookup'
				}
			}
		});
	}
}

