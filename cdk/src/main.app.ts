<#assign nameCcPrefix = name?replace(" ", "")>
<#assign nameDcPrefix = name?replace(" [a-zA-Z]", "-$0", 'r')?replace(" ", "")?lower_case>
#!/usr/bin/env node

import { App } from 'aws-cdk-lib';
import 'source-map-support/register';
import { CODECOMMIT_REPO_NAME, DEV_OPS_ACCOUNT, REGION } from './configuration';
import { PipelineStack } from './pipeline/pipeline.stack';

const app = new App();

new PipelineStack(app, '${nameDcPrefix}-pipeline-stack', {
	stackName: '${nameCcPrefix}PipelineStack',
	repositoryName: CODECOMMIT_REPO_NAME,
	env: {
		account: DEV_OPS_ACCOUNT,
		region: REGION
	}
});
