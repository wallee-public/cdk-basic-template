/**
 * Creates a Docker image tag from resolved commit hash and current timestamp.
 * Format: {commit hash}-{current timestamp}
 *
 * @return the docker image tag.
 */
 export const createDockerImageTag = (): string => {
	const commitHash = process.env.CODEBUILD_RESOLVED_SOURCE_VERSION;
	const currentTimestamp = Date.now();

	return `${r"${commitHash?.slice(0, 8) ?? currentTimestamp}"}-${r"${currentTimestamp}"}`;
};
